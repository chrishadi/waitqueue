# Wait Queue
***

## About
A [Cloudflare Waiting Room](https://www.cloudflare.com/application-services/products/waiting-room/)
inspired REST API for virtual queue. This implementation uses [JSON:API](jsonapi.org) formatted payload
to send the data between the client and the server, but it is also possible to use HTTP headers -
or cookie, to carry the payload.

This version also serves as a proof-of-concept for comparing between an all-in system and a system
that uses dedicated database services. 

In order to get the most optimal performance for retrieving the queue ticket position at any instance
of time, this solution uses a special variant of skip-list also known as Redis ZSet (sorted set)
data structure. The reason is that its time complexity for retrieving the member's rank is O(log(N)).

## TODO

- add tests
- add ci.yml
