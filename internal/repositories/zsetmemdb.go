package repositories

import (
	"context"
	"sync"
	"time"

	"github.com/chrishadi/waitqueue/internal/models"
	"github.com/hashicorp/go-memdb"
	"github.com/liyiheng/zset"
)

const (
	tableName          = "tickets"
	primaryIdxName     = "id"
	visitorUUIDIdxName = "visitorUUID"
)

type ZSetMemDB struct {
	zSet  *zset.SortedSet[string]
	memDB *memdb.MemDB
	mutex sync.Mutex
}

var _ TicketsRepository = (*ZSetMemDB)(nil)

func NewZSetMemDB() (*ZSetMemDB, error) {
	schema := &memdb.DBSchema{
		Tables: map[string]*memdb.TableSchema{
			tableName: {
				Name: tableName,
				Indexes: map[string]*memdb.IndexSchema{
					primaryIdxName: {
						Name:    primaryIdxName,
						Unique:  true,
						Indexer: &memdb.UUIDFieldIndex{Field: "UUID"},
					},
					visitorUUIDIdxName: {
						Name:    visitorUUIDIdxName,
						Unique:  true,
						Indexer: &memdb.UUIDFieldIndex{Field: "VisitorUUID"},
					},
				},
			},
		},
	}

	db, err := memdb.NewMemDB(schema)
	if err != nil {
		return nil, err
	}

	return &ZSetMemDB{
		zSet:  zset.New[string](),
		memDB: db,
	}, nil
}

func (r *ZSetMemDB) FindOrInsertTicket(_ context.Context, payload *models.Ticket) (*models.Ticket, error) {
	txn := r.memDB.Txn(true)
	defer txn.Abort()

	raw, err := txn.First(tableName, visitorUUIDIdxName, payload.VisitorUUID)
	if err != nil {
		return nil, err
	}

	if raw != nil {
		record := raw.(*models.Ticket)
		rank, _ := r.zSet.GetRank(record.UUID, true)

		return &models.Ticket{
			UUID:        record.UUID,
			VisitorUUID: record.VisitorUUID,
			Position:    rank + 1,
		}, nil
	}

	unixMicro := time.Now().UnixMicro()

	if err = txn.Insert(tableName, &models.Ticket{
		UUID:        payload.UUID,
		VisitorUUID: payload.VisitorUUID,
		CreatedAt:   unixMicro,
	}); err != nil {
		return nil, err
	}

	txn.Commit()

	r.mutex.Lock()
	r.zSet.Set(float64(unixMicro), payload.UUID)
	length := r.zSet.Length()
	r.mutex.Unlock()

	return &models.Ticket{
		UUID:        payload.UUID,
		VisitorUUID: payload.VisitorUUID,
		Position:    length,
	}, nil
}

func (r *ZSetMemDB) FindTicket(_ context.Context, ticketUUID string) (*models.Ticket, error) {
	txn := r.memDB.Txn(false)
	defer txn.Abort()

	raw, err := txn.First(tableName, primaryIdxName, ticketUUID)
	if err != nil {
		return nil, err
	}

	if raw == nil {
		return nil, nil
	}

	record := raw.(*models.Ticket)
	rank, _ := r.zSet.GetRank(ticketUUID, false)

	return &models.Ticket{
		UUID:        ticketUUID,
		VisitorUUID: record.VisitorUUID,
		Position:    rank + 1,
	}, nil
}

func (r *ZSetMemDB) RemoveTicket(_ context.Context, ticketUUID string) (*models.Ticket, error) {
	txn := r.memDB.Txn(true)
	defer txn.Abort()

	raw, err := txn.First(tableName, primaryIdxName, ticketUUID)
	if err != nil {
		return nil, err
	}

	if raw == nil {
		return nil, nil
	}

	err = txn.Delete(tableName, &models.Ticket{UUID: ticketUUID})
	if err != nil {
		return nil, err
	}

	txn.Commit()

	r.mutex.Lock()
	r.zSet.Delete(ticketUUID)
	r.mutex.Unlock()

	return raw.(*models.Ticket), nil
}
