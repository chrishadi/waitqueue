package repositories

import (
	"context"

	"github.com/chrishadi/waitqueue/internal/models"
)

type TicketsRepository interface {
	FindOrInsertTicket(ctx context.Context, payload *models.Ticket) (*models.Ticket, error)
	FindTicket(ctx context.Context, ticketUUID string) (*models.Ticket, error)
	RemoveTicket(ctx context.Context, ticketUUID string) (*models.Ticket, error)
}
