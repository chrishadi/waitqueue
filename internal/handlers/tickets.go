package handlers

import (
	"fmt"
	"log"
	"net/http"
	"strconv"

	"github.com/chrishadi/waitqueue/internal/models"
	"github.com/chrishadi/waitqueue/internal/services"
	"github.com/google/jsonapi"
	"github.com/google/uuid"
	"github.com/gorilla/mux"
)

// TicketsHandler processes requests for the "/tickets" path.
type TicketsHandler struct {
	service *services.TicketsService
}

// NewTicketsHandler constructs a TicketsHandler.
func NewTicketsHandler(service *services.TicketsService) *TicketsHandler {
	return &TicketsHandler{
		service: service,
	}
}

func (h *TicketsHandler) HandleCreateTicket(w http.ResponseWriter, r *http.Request) {
	var payload models.TicketPayload
	if err := jsonapi.UnmarshalPayload(r.Body, &payload); err != nil {
		writeError(w, http.StatusBadRequest, "Cannot decode request payload: %s", err)
		return
	}

	visitorUUID, err := uuid.Parse(payload.VisitorUUID)
	if err != nil {
		writeError(w, http.StatusBadRequest, "Invalid 'visitorId' format: %s", err)
		return
	}

	ticket, err := h.service.GetOrCreateTicket(r.Context(), visitorUUID.String())

	writeJSONAPI(w, http.StatusCreated, ticket)
}

func (h *TicketsHandler) HandleGetTicket(w http.ResponseWriter, r *http.Request) {
	ticketUUID, err := getUUIDPathVar(r)
	if err != nil {
		writeError(w, http.StatusBadRequest, "Invalid ticket: %s", err)
		return
	}

	ticket, err := h.service.GetTicket(r.Context(), ticketUUID)
	if err != nil {
		writeError(w, http.StatusInternalServerError, "Error getting ticket %s: %s", ticketUUID, err)
		return
	}

	if ticket == nil {
		writeError(w, http.StatusNotFound, "Ticket %s does not exist", ticketUUID)
		return
	}

	writeJSONAPI(w, http.StatusOK, ticket)
}

func (h *TicketsHandler) HandleDeleteTicket(w http.ResponseWriter, r *http.Request) {
	ticketUUID, err := getUUIDPathVar(r)
	if err != nil {
		writeError(w, http.StatusBadRequest, "Invalid ticket: %s", err)
		return
	}

	ticket, err := h.service.RemoveTicket(r.Context(), ticketUUID)
	if err != nil {
		writeError(w, http.StatusInternalServerError, "Error removing ticket: %s", err)
		return
	}

	if ticket == nil {
		writeError(w, http.StatusNotFound, "Ticket %s does not exist", ticketUUID)
		return
	}

	writeJSONAPI(w, http.StatusOK, ticket)
}

func getUUIDPathVar(r *http.Request) (string, error) {
	uuidVar := mux.Vars(r)["uuid"]
	uuidObj, err := uuid.Parse(uuidVar)

	return uuidObj.String(), err
}

func writeError(w http.ResponseWriter, statusCode int, format string, args ...any) {
	writeHeader(w, statusCode)

	if statusCode == http.StatusInternalServerError {
		log.Printf(format, args...)
		format = "Unexpected error occurred"
	}

	errs := []*jsonapi.ErrorObject{
		{
			Detail: fmt.Sprintf(format, args),
			Status: strconv.Itoa(statusCode),
		},
	}

	if err := jsonapi.MarshalErrors(w, errs); err != nil {
		log.Print("Error writing response: ", err)
		http.Error(w, err.Error(), statusCode)
	}
}

func writeJSONAPI(w http.ResponseWriter, statusCode int, v any) {
	writeHeader(w, statusCode)

	if err := jsonapi.MarshalPayload(w, v); err != nil {
		log.Print("Error writing response: ", err)
		http.Error(w, err.Error(), statusCode)
	}
}

func writeHeader(w http.ResponseWriter, statusCode int) {
	w.Header().Set("Content-Type", jsonapi.MediaType)

	if statusCode != http.StatusOK {
		w.WriteHeader(statusCode)
	}
}
