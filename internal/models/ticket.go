package models

type Ticket struct {
	UUID        string `jsonapi:"primary,tickets"`
	VisitorUUID string `jsonapi:"attr,visitorId"`
	Position    int64  `jsonapi:"attr,position"`
	IsClaimable bool   `jsonapi:"attr,isClaimable"`
	CreatedAt   int64
}

type TicketPayload struct {
	VisitorUUID string `jsonapi:"attr,visitorId"`
}
