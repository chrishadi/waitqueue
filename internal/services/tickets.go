package services

import (
	"context"

	"github.com/chrishadi/waitqueue/internal/models"
	"github.com/chrishadi/waitqueue/internal/repositories"
	"github.com/google/uuid"
)

// TicketsService contains business logic related to ticket processing.
type TicketsService struct {
	repository repositories.TicketsRepository
}

// NewTicketsService constructs a TicketService.
func NewTicketsService(repository repositories.TicketsRepository) *TicketsService {
	return &TicketsService{
		repository: repository,
	}
}

func (s *TicketsService) GetOrCreateTicket(ctx context.Context, visitorUUID string) (*models.Ticket, error) {
	newUUID, err := uuid.NewV7()
	if err != nil {
		return nil, err
	}

	return s.repository.FindOrInsertTicket(ctx, &models.Ticket{
		UUID:        newUUID.String(),
		VisitorUUID: visitorUUID,
	})
}

func (s *TicketsService) GetTicket(ctx context.Context, ticketUUID string) (*models.Ticket, error) {
	return s.repository.FindTicket(ctx, ticketUUID)
}

func (s *TicketsService) RemoveTicket(ctx context.Context, ticketUUID string) (*models.Ticket, error) {
	return s.repository.RemoveTicket(ctx, ticketUUID)
}
