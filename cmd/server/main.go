package main

import (
	"context"
	"log"
	"net/http"
	"os"
	"os/signal"
	"time"

	"github.com/chrishadi/waitqueue/internal/handlers"
	"github.com/chrishadi/waitqueue/internal/repositories"
	"github.com/chrishadi/waitqueue/internal/services"
	"github.com/gorilla/mux"
)

const connTimeout = 15 * time.Second

func main() {
	ticketsRepository, err := repositories.NewZSetMemDB()
	if err != nil {
		log.Fatal("Error initiating ZSetMemDB repository: ", err)
	}

	ticketsService := services.NewTicketsService(ticketsRepository)
	ticketsHandler := handlers.NewTicketsHandler(ticketsService)

	router := mux.NewRouter()
	registerTicketsRoute(router, ticketsHandler)

	http.Handle("/", router)

	srv := http.Server{
		Addr:         ":8080",
		Handler:      router,
		ReadTimeout:  connTimeout,
		WriteTimeout: connTimeout,
	}

	go func() {
		sigChan := make(chan os.Signal, 1)
		signal.Notify(sigChan, os.Interrupt)
		<-sigChan

		log.Print("Shutting down server")
		if err := srv.Shutdown(context.Background()); err != nil {
			log.Fatal("Error shutting down server: ", err)
		}
	}()

	log.Print("Listening at: ", srv.Addr)
	log.Fatal(srv.ListenAndServe())
}

func registerTicketsRoute(router *mux.Router, handler *handlers.TicketsHandler) {
	route := router.PathPrefix("/api/v1/tickets").Subrouter()
	route.Methods(http.MethodPost).HandlerFunc(handler.HandleCreateTicket)

	subRoute := route.PathPrefix("/{uuid}").Subrouter()
	subRoute.Methods(http.MethodGet).HandlerFunc(handler.HandleGetTicket)
	subRoute.Methods(http.MethodDelete).HandlerFunc(handler.HandleDeleteTicket)
}
